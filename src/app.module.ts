import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Game24Module } from './game24/game24.module';
import { PreauthMiddleware } from './auth/preauth.middleware';
import { SearchApiModule } from './search-api/search-api.module';
import { ConfigModule } from '@nestjs/config';
@Module({
  imports: [Game24Module, SearchApiModule, ConfigModule.forRoot()],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(PreauthMiddleware).forRoutes({
      path: '*',
      method: RequestMethod.ALL,
    });
  }
}
