import { ApiProperty } from '@nestjs/swagger';
import {
  ArrayMaxSize,
  ArrayMinSize,
  IsArray,
  IsNotEmpty,
} from 'class-validator';
export class Game4RequestDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsArray()
  @ArrayMaxSize(4)
  @ArrayMinSize(4)
  gameNumberList: number[];
}
