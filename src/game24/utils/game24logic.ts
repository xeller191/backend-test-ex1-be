type Operator = '+' | '-' | '*' | '/';

export class Calculate24 {
  private results: string[] = [];
  private usedNumbers: boolean[] = [];

  private calculateRecursive(
    currentNumbers: number[],
    currentExpression: string,
    target: number,
  ): void {
    if (currentNumbers.length === 1) {
      const finalNumber = currentNumbers[0];
      if (finalNumber === target) {
        this.results.push(currentExpression);
      }
      return;
    }

    for (let i = 0; i < currentNumbers.length; i++) {
      if (this.usedNumbers[i]) {
        continue; // ตัวเลขนี้ถูกใช้ไปแล้ว
      }

      for (let j = 0; j < currentNumbers.length; j++) {
        if (i !== j && !this.usedNumbers[j]) {
          const newNumbers = [...currentNumbers];
          const operand1 = newNumbers.splice(i, 1)[0];
          const operand2 = newNumbers.splice(j - (j > i ? 1 : 0), 1)[0];

          for (const operator of ['+', '-', '*', '/'] as Operator[]) {
            if (operator === '/' && operand2 === 0) {
              continue; // ไม่ทำการหารด้วย 0
            }

            const newExpression =
              currentExpression === ''
                ? `(${operand1} ${operator} ${operand2})`
                : `(${currentExpression} ${operator} ${operand1} ${operator} ${operand2})`;

            const newResult =
              operator === '+'
                ? operand1 + operand2
                : operator === '-'
                  ? operand1 - operand2
                  : operator === '*'
                    ? operand1 * operand2
                    : operand1 / operand2;

            this.usedNumbers[i] = true;
            this.usedNumbers[j] = true;
            this.calculateRecursive(
              [...newNumbers, newResult],
              newExpression,
              target,
            );
            this.usedNumbers[i] = false;
            this.usedNumbers[j] = false;
          }
        }
      }
    }
  }

  public calculate(numbers: number[], target: number): string[] {
    this.results = [];
    this.usedNumbers = new Array(numbers.length).fill(false);

    this.calculateRecursive(numbers, '', target);

    return this.results;
  }
}
