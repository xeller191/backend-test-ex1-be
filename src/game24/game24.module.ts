import { Module } from '@nestjs/common';
import { Game24Service } from './game24.service';
import { Game24Controller } from './game24.controller';

@Module({
  providers: [Game24Service],
  controllers: [Game24Controller],
})
export class Game24Module {}
