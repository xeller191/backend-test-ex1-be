import { Injectable } from '@nestjs/common';
import { Game4ResponseDto } from './dto/response/game24-response.dto';
import { plainToInstance } from 'class-transformer';
import { Game4RequestDto } from './dto/request/game24-request.dto';
import { Calculate24 } from './utils/game24logic';
@Injectable()
export class Game24Service {
  game24result(body: Game4RequestDto): Game4ResponseDto {
    // ตัวอย่างการใช้งาน
    // const gameSolver = new Game24Solver(
    //   body.gameNumberList[0],
    //   body.gameNumberList[1],
    //   body.gameNumberList[2],
    //   body.gameNumberList[3],
    // );
    // ตัวอย่างการใช้งาน
    const calculator = new Calculate24();
    const numbers = [4, 2, 1, 8];
    const target = 24;
    const solutions = calculator.calculate(numbers, target);

    console.log(
      `หากใช้ตัวเลข ${numbers} และดำเนินการทางคณิตศาสตร์เพื่อให้ได้ผลลัพธ์ ${target}`,
    );
    console.log(`ผลลัพธ์ทั้งหมด: `, solutions);

    return plainToInstance(Game4ResponseDto, { result: 'NO' });
  }
}
