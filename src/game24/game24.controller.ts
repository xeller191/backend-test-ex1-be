import { Body, Controller, Post } from '@nestjs/common';
import { Game24Service } from './game24.service';
import { Game4RequestDto } from './dto/request/game24-request.dto';

@Controller('game24')
export class Game24Controller {
  constructor(private service: Game24Service) {}
  @Post()
  async create(@Body() body: Game4RequestDto) {
    console.log({ body });
    return this.service.game24result(body);
  }
}
