export class SearchApiResponseDto {
  business_status: string;
  formatted_address: string;
  geometry: Geometry;
  icon: string;
  icon_background_color: string;
  icon_mask_base_uri: string;
  name: string;
  opening_hours?: OpeningHours;
  photos?: Photo[];
  place_id: string;
  price_level?: number;
  rating: number;
  reference: string;
  types: string[];
  user_ratings_total: number;
  plus_code?: PlusCode;
}

class Geometry {
  location: Location;
  viewport: Viewport;
}

class Location {
  lat: number;
  lng: number;
}

class Viewport {
  northeast: Location;
  southwest: Location;
}
class OpeningHours {
  open_now: boolean;
}

class Photo {
  height: number;
  html_attributions: string[];
  photo_reference: string;
  width: number;
}

class PlusCode {
  compound_code: string;
  global_code: string;
}
