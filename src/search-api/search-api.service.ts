import { Injectable } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { SearchApiResponseDto } from './dto/response/search-api-response.dto';
import axios from 'axios';
@Injectable()
export class SearchApiService {
  async googleSearchAPi(search: string) {
    const apiKey = process.env.API_KEY;
    const response = await axios.get(
      `https://maps.googleapis.com/maps/api/place/textsearch/json?query=${search}&key=${apiKey}`,
    );
    return plainToInstance(SearchApiResponseDto, {
      result: response.data.results,
    });
  }
}
