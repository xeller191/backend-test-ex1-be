import { Controller, Get, Query } from '@nestjs/common';
import { SearchApiService } from './search-api.service';

@Controller('search-api')
export class SearchApiController {
  constructor(private service: SearchApiService) {}

  @Get() //http://localhost:3000/api/v1/search-api?search=art  //{ search: 'art' }
  async create(@Query('search') search: string) {
    return this.service.googleSearchAPi(search);
  }
}
