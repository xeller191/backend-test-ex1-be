import { Module } from '@nestjs/common';
import { SearchApiService } from './search-api.service';
import { SearchApiController } from './search-api.controller';

@Module({
  providers: [SearchApiService],
  controllers: [SearchApiController],
})
export class SearchApiModule {}
