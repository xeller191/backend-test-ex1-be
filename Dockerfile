FROM node:18.13.0-buster-slim
WORKDIR /usr/src/app
COPY ["package.json", "package-lock.json", "./"]
RUN npm install
COPY . .
RUN npm run build
EXPOSE 8000
CMD ["node", "dist/main" ]

# 1.build image
# docker build -t ex1-api -f Dockerfile .

# 2.run image
# docker run -p 8000:8000 --name ex1-api-container ex1-api

# optional ใช้ docker-compose :
# docker-compose up


# up docker image to doceer hub  ต้อง docker login ก่อน
# 1. build image จากข้อ 1
# 2.1 สร้าง tage -> $ docker tag ex1-api user-name-docker-hub/docker-image:tag 
# 2.2 $ docker tag ex1-api chaiyaphat12079/ex1-api:1.0.0 
# 3. push image -> $ docker push chaiyaphat12079/ex1-api:1.0.0
# 4.1  test pull และ รัน image ->   docker run -p 8000:8000 --name  <NameContainer> <Image>
# 4.2 $ docker run -p 8000:8000 --name  ex1-api chaiyaphat12079/ex1-api:1.0.0